extern NSString* const PartnerApplicationSecretUUIDKey;
extern NSString* const PartnerApplicationInfoKey;
extern NSString* const PartnerApplicationIdentifierKey;
extern NSString* const PartnerApplicationDocumentExtensionKey;
extern NSString* const PartnerApplicationDocumentUTIKey;
extern NSString* const PartnerApplicationDocumentExtension;
extern NSString* const PartnerApplicationDocumentUTI;

#import "QOPartnerApplicationAnnotationKeys.h"

NSString* const PartnerApplicationSecretUUIDKey = @"PartnerApplicationSecretUUID";
NSString* const PartnerApplicationInfoKey = @"PartnerApplicationInfo";
NSString* const PartnerApplicationIdentifierKey = @"PartnerApplicationIdentifier";

NSString* const PartnerApplicationDocumentExtension = @"alf01";
NSString* const PartnerApplicationDocumentExtensionKey = @"PartnerApplicationDocumentExtension";

NSString* const PartnerApplicationDocumentUTI = @"com.alfresco.mobile.qpa";
NSString* const PartnerApplicationDocumentUTIKey = @"PartnerApplicationDocumentUTI";

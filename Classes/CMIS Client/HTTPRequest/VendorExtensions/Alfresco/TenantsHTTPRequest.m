/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is the Alfresco Mobile App.
 *
 * The Initial Developer of the Original Code is Zia Consulting, Inc.
 * Portions created by the Initial Developer are Copyright (C) 2011-2012
 * the Initial Developer. All Rights Reserved.
 *
 *
 * ***** END LICENSE BLOCK ***** */
//
//  NetworksHTTPRequest.m
//

#import "TenantsHTTPRequest.h"
#import "SBJSON.h"

NSString * const kPaidBussinesClassName = @"PAID_BUSINESS"; 

@implementation TenantsHTTPRequest
@synthesize jsonObject;
@synthesize primaryTenantID;
@synthesize secondaryTenantIDs;
@synthesize allTenantIDs;
@synthesize paidAccount;

- (void)dealloc
{
    [jsonObject release];
    [primaryTenantID release];
    [secondaryTenantIDs release];
    [allTenantIDs release];
    
    [super dealloc];
}

+ (id)tenantsRequestForAccountUUID:(NSString *)uuid
{
    NSLog(@"Instance of TenantsHTTPRequest created with account UUID: %@", uuid);
    
    return [self requestForServerAPI:kServerAPINetworksCollection accountUUID:uuid];
}

#pragma mark - 
#pragma mark ASIHTTPRequestDelegate methods

- (void)requestFinishedWithSuccessResponse
{
    _GTMDevLog(@"Tenants response: %@", [self responseString]);
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];    
	NSArray *result = [jsonParser objectWithString:[self responseString]];
    [jsonParser release];
    
    [self setJsonObject:result];
    [self setPrimaryTenantID:[result valueForKeyPath:@"data.home.tenant"]];
    [self setSecondaryTenantIDs:[result valueForKeyPath:@"data.secondary.tenant"]];
    [self setAllTenantIDs:[[NSArray arrayWithObject:self.primaryTenantID] arrayByAddingObjectsFromArray:self.secondaryTenantIDs]];
    
    NSString *className = [result valueForKeyPath:@"data.home.className"];
    [self setPaidAccount:[className isEqualToString:kPaidBussinesClassName]];
}

- (void)failWithError:(NSError *)theError
{
    [super failWithError:theError];    
}

@end
